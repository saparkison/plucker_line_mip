using LinearAlgebra

function PluckerLines(line_3D_end_pts)

  pts_start = line_3D_end_pts[:, 1:3]
  pts_end = line_3D_end_pts[:, 4:6]


  u = zeros(eltype(pts_start), size(pts_start))
  v = zeros(eltype(pts_start), size(pts_start))
  for i = 1:size(pts_start,1)
    u[i,:] = (LinearAlgebra.cross(pts_start[i,:], pts_end[i,:]))'
    v[i,:] = (pts_end[i,:] - pts_start[i,:])'
  end

  return hcat(u, v)

end
