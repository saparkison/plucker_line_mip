using Images, ImageDraw, Statistics, LinearAlgebra, ImageMorphology, Random, DelimitedFiles, Printf

function get_random_color(seed)
  if seed == 0
    return RGB(0, 0, 0)
  else
    Random.seed!(seed)
    return rand(RGB{N0f8})
  end
end

function run()

  for frame = 1:999
    string_num = @sprintf "%d" frame
    image = load(string("/home/stevenparkison/datasets/tri/sparkison-data-mapping/data_20190909/", string_num, ".png"))
    label_image = load("/home/stevenparkison/datasets/tri/sparkison-data-mapping/data_20190909/" * string_num * "_koinet_output_labels.png")
    label_image = reinterpret(UInt8, label_image)
    label_set = [2,13,19]


    binary_image = UInt8.(isequal.(label_image, 2) .+ isequal.(label_image, 13) .+ isequal.(label_image, 19))
    binary_image = erode(binary_image)
    binary_image[1:500, :] .= 0
    save("binary_image.png", UInt8.(binary_image.*255))

    conponent_image = Images.label_components(binary_image)
    conponent_set = Set(conponent_image)

    line_points = Array{Int, 2}(undef, 0, 4)
    for c in conponent_set
      if c == 0
        continue
      end
      index_list = findall(x->x==c, conponent_image)
      if size(index_list,1) < 50
        continue
      end
      index_mat = zeros(size(index_list,1),2)
      for i = 1:size(index_list,1)
        index_mat[i,1] = index_list[i][1]
        index_mat[i,2] = index_list[i][2]
      end

      index_cov = cov(index_mat)

      s = svd(index_cov)

      line_direction = s.U[:,1]
      line_point = [mean(index_mat[:,1]), mean(index_mat[:,2])]

      dist = zeros(size(index_list,1),1)
      for i = 1:size(index_list,1)
        dist[i] = dot(index_mat[i,:]-line_point, line_direction)
      end

      start_point = (line_point + minimum(dist).*line_direction)
      end_point = (line_point + maximum(dist).*line_direction)

      start_point = round.(Int, start_point)
      end_point = round.(Int, end_point)

      start_point[1] = clamp(start_point[1], 1, 1216)
      start_point[2] = clamp(start_point[2], 1, 1936)
      end_point[1] = clamp(end_point[1], 1, 1216)
      end_point[2] = clamp(end_point[2], 1, 1936)

      line_points = vcat(line_points, [start_point[2]; start_point[1]; end_point[2]; end_point[1]]')

      draw!(image, LineSegment(Point(start_point[2], start_point[1]), Point(end_point[2], end_point[1])), RGB{N0f8}(0.0, 1.0, 0.0))

    end

    open("/home/stevenparkison/datasets/tri/sparkison-data-mapping/data_20190909/" * string_num *"_semseg_lines.txt", "w") do io
      writedlm(io, line_points,' ')
    end

    save("output.png", image)
    save("segements.png", map(x->get_random_color(x), conponent_image))
  end
end

run()
