using JuMP, Gurobi, Profile, CSV, LinearAlgebra, Rotations, DataFrames, Gadfly, Cairo, Fontconfig, StatsBase

include("fit_rotation.jl")

function transform_est(data_size::Int, noise::Float64, num_outlier::Int = 0; rotation_relaxation = :Domain)

  data = rand(3,data_size)
  similarity_matrix = zeros(data_size,data_size)


  R_gt = rand(RotMatrix{3})
  t_gt = rand(3)
  perm = sample(1:data_size, data_size, replace = false)
  new_data = R_gt*data[:,perm] + t_gt*ones(1,data_size) + randn(3,data_size) .*noise
  new_data = hcat(new_data, rand(3,num_outlier))
  new_data_size = data_size + num_outlier

  start = time_ns()
  optimizer = Gurobi.Optimizer
  model = Model(with_optimizer(optimizer, Threads = 50))

  # switch constraints
  @variable(model, switch[1:data_size, 1:new_data_size], Bin)
  for i = 1:data_size
      @constraint(model, sum(switch[i,:]) >= new_data_size-1)
  end
  for i = 1:new_data_size
      @constraint(model, sum(switch[:,i]) >= data_size-1)
  end

  # translation and rotation variables
  @variable(model, -1 <= R[1:3, 1:3] <= 1)
  if rotation_relaxation == :Domain
    # Bound R to have row and column L1 norm <= sqrt(3), since
    # row and column L2 norm = 2.
    # (We can't do lower bound L1 norm >= 1 b/c that's not
    # a convex constraint)
    @variable(model, R_abs[1:3, 1:3] >= 0 )
    @constraint(model, R_abs .>= R)
    @constraint(model, R_abs .>= -R)
    for i in 1:3
      @constraint(model, [1 1 1] * R_abs[:, i] .<= sqrt(3))
      @constraint(model, [1 1 1] * R_abs[i, :] .<= sqrt(3))
    end
  elseif rotation_relaxation == :RtRBilinear
    # Orthogonality
    @constraint(model, transpose(R) * R .== Matrix{Float64}(I, 3, 3))
    #@constraint(m, R * transpose(R) .== eye(3, 3))
    # Forcing right-handedness of coordinates via cross products
    # (which should result in det(R) = +1)
    for i in 0:2
      @constraint(model, LinearAlgebra.cross(R[(i+0)%3+1, :], R[(i+1)%3+1, :]) .== R[(i+2)%3+1, :])
      #@constraint(m, cross(R[:, (i+0)%3+1], R[:, (i+1)%3+1]) .== R[:, (i+2)%3+1])
    end
    relaxbilinear!(model, method=:Logarithmic2D, disc_level=4)
  end

  @variable(model, -1 <= t[1:3] <= 1)


  # alpha constraint, slack variable
  @variable(model, alpha[1:data_size,1:new_data_size])
  @constraint(model, alpha .>= 0)
  for i = 1:data_size
    for j = 1:new_data_size
      @constraint(model, new_data[:,j] - R*data[:,i] - t  .-(1000*switch[i,j]) .<= alpha[i,j] )
      @constraint(model, new_data[:,j] - R*data[:,i] - t  .+(1000*switch[i,j]) .>= -alpha[i,j] )
    end
  end

  @objective(model, Min, sum(alpha) + sum(switch))

  println("\nOptimizing")
  JuMP.optimize!(model)
  stop = time_ns()
  println("Elaped time")
  println((stop-start)/1e6)

  final_switch = JuMP.value.(switch)
  associations = zeros(data_size, 1)
  for i = 1:data_size
    for j = 1:data_size
      if final_switch[i,j] < 1e-4
        associations[j] = i
      end
    end
  end
  println(perm)
  println(associations)
  R_final = FitRotation(JuMP.value.(R))
  t_final = JuMP.value.(t)

  return (stop-start)/1e6, AngleAxis(R_final'*R_gt).theta, norm(t_final-t_gt), data_size - sum(associations.==perm)
end


df = DataFrame(time_ms = Float64[], num_points= Int[], rad_error=Float64[], m_error=Float64[], association_error = Int[])

for num = 10:2:20
  for i = 1:10
    time, rad_e, m_e, a_e = transform_est(num, 1e-3)
    push!(df, [time, num, rad_e, m_e, a_e])
    CSV.write("point_size_results.csv", df)
  end
end


p1 = plot(df, x="num_points", y="time_ms", Geom.boxplot(suppress_outliers=true))
p2 = plot(df, x="num_points", y="rad_error", Geom.boxplot(suppress_outliers=true))
p3 = plot(df, x="num_points", y="m_error", Geom.boxplot(suppress_outliers=true))
p4 = plot(df, x="num_points", y="association_error", Geom.boxplot(suppress_outliers=true))
draw(PNG("point_size_results.png", 8inch, 8inch),vstack(hstack(p1,p2),hstack(p3,p4)))

df = DataFrame(time_ms = Float64[], num_outliers= Int[], rad_error=Float64[], m_error=Float64[], association_error = Int[])
for num = 2:2:10
  for i = 1:10
    time, rad_e, m_e, a_e = transform_est(15, 1e-3, num)
    push!(df, [time, num, rad_e, m_e, a_e])
    CSV.write("point_outliers_results.csv", df)
  end
end

p1 = plot(df, x="num_outliers", y="time_ms", Geom.boxplot(suppress_outliers=true))
p2 = plot(df, x="num_outliers", y="rad_error", Geom.boxplot(suppress_outliers=true))
p3 = plot(df, x="num_outliers", y="m_error", Geom.boxplot(suppress_outliers=true))
p4 = plot(df, x="num_outliers", y="association_error", Geom.boxplot(suppress_outliers=true))
draw(PNG("point_outliers_results.png", 8inch, 8inch),vstack(hstack(p1,p2),hstack(p3,p4)))
