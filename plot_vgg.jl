using CSV, StatsPlots

pyplot()
df = CSV.read("results/vgg/sampled/results.csv")
df[:num_points] = string.(collect(df[:num_points]))
plot(
(@df df groupedboxplot(:num_points, :time_ms, group=:constraint_method,
                       ylabel = "Time (ms)", yscale = :log10,
                       xlabel = "Number of Lines", legend=:none)),
(@df df groupedboxplot(:num_points, :rad_error, group=:constraint_method,
                       ylabel = "Angle Error (radians)", ylims = (0,0.1),
                       xlabel = "Number of Lines", legend=:none)),
(@df df groupedboxplot(:num_points, :m_error, group=:constraint_method,
                       ylabel = "Translation Error (m)", ylims = (0,1),
                       xlabel = "Number of Lines")),
#(@df df groupedboxplot(:num_points, :association_error, group=:constraint_method,
#                       ylabel = "Mis-association",
#                       xlabel = "Number of Lines",legend=:none)))
layout = (1,3), size = (800, 400))
savefig("results/vgg/sampled/boxplot.png")

