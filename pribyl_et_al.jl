using Profile, CSV, LinearAlgebra, Statistics, Rotations, DataFrames, Printf, StatsBase, FileIO

include("plucker_lines.jl")
include("closest_point_to_lines.jl")
include("homogenous_lines.jl")
include("get_transform.jl")
include("kr_from_p.jl")
include("results_figure.jl")

function ConstructPluckerTransformation( R, t )
  skew_t = zeros(3,3)
  skew_t[3,2] = -t[1]
  skew_t[1,3] = -t[2]
  skew_t[2,1] = -t[3]

  skew_t = skew_t + skew_t'

  return hcat(R, R*skew_t)
end


function PribylEtAl( line_3d_endpts, line_2d_endpts, K, R, t; z_direction = -1 )

    num_3d_points = size(line_3d_endpts,1)
    num_2d_points = size(line_2d_endpts,1)

    center_3d = mean( [line_3d_endpts[:,1:3]; line_3d_endpts[:,4:6]], dims = 1)
    closest_point = ClosestPointToLines(line_3d_endpts)
    shifted_line_3d = zeros(size(line_3d_endpts))
    shifted_line_3d[:,1:3] = line_3d_endpts[:,1:3] - ones(num_3d_points,1) * closest_point'
    shifted_line_3d[:,4:6] = line_3d_endpts[:,4:6] - ones(num_3d_points,1) * closest_point'
    plucker_3d = PluckerLines(shifted_line_3d)'

    norm_2d = HomogenousLines( line_2d_endpts, K)
   flatened_2d = hcat(norm_2d[1:3,:], norm_2d[4:6,:])

    # Prenormalize 2D Lines
    shift_2d = mean( [norm_2d[1,:]./norm_2d[3,:] norm_2d[2,:]./norm_2d[3,:]], dims = 1)
    pre_shift_2D_lines = [1 0 -shift_2d[1]; 0 1 -shift_2d[2]; 0 0 1]
    lines_2D_shift = pre_shift_2D_lines * norm_2d[1:3,:]

    scale_2D = sqrt(2) / mean(((lines_2D_shift[1,:] ./ lines_2D_shift[3,:]).^2 + (lines_2D_shift[2,:] ./ lines_2D_shift[3,:]).^2).^0.5)
    pre_scale_2D_lines = diagm(0 => [scale_2D; scale_2D; 1])
    pre_tform_2D_lines = pre_scale_2D_lines * pre_shift_2D_lines
    println(pre_tform_2D_lines)

    norm_2d[1:3,:] = pre_tform_2D_lines * norm_2d[1:3,:]
    norm_2d[4:6,:] = pre_tform_2D_lines * norm_2d[4:6,:]


    #println("3d Pts")
    #println(plucker_3d)
    #println("2d Pts")
    #println(norm_2d)

    println(size(norm_2d))
    println(size(plucker_3d))
    println(num_3d_points)
    println(size(norm_2d[3,:]))

    start = time_ns()
    M = vcat( hcat( norm_2d[3,:].*plucker_3d[1,:], zeros(num_3d_points), -norm_2d[1,:].*plucker_3d[1,:],
                    norm_2d[3,:].*plucker_3d[2,:], zeros(num_3d_points), -norm_2d[1,:].*plucker_3d[2,:],
                    norm_2d[3,:].*plucker_3d[3,:], zeros(num_3d_points), -norm_2d[1,:].*plucker_3d[3,:],
                    norm_2d[3,:].*plucker_3d[4,:], zeros(num_3d_points), -norm_2d[1,:].*plucker_3d[4,:],
                    norm_2d[3,:].*plucker_3d[5,:], zeros(num_3d_points), -norm_2d[1,:].*plucker_3d[5,:],
                    norm_2d[3,:].*plucker_3d[6,:], zeros(num_3d_points), -norm_2d[1,:].*plucker_3d[6,:]),
              hcat( zeros(num_3d_points), norm_2d[3,:].*plucker_3d[1,:], -norm_2d[2,:].*plucker_3d[1,:],
                    zeros(num_3d_points), norm_2d[3,:].*plucker_3d[2,:], -norm_2d[2,:].*plucker_3d[2,:],
                    zeros(num_3d_points), norm_2d[3,:].*plucker_3d[3,:], -norm_2d[2,:].*plucker_3d[3,:],
                    zeros(num_3d_points), norm_2d[3,:].*plucker_3d[4,:], -norm_2d[2,:].*plucker_3d[4,:],
                    zeros(num_3d_points), norm_2d[3,:].*plucker_3d[5,:], -norm_2d[2,:].*plucker_3d[5,:],
                    zeros(num_3d_points), norm_2d[3,:].*plucker_3d[6,:], -norm_2d[2,:].*plucker_3d[6,:]))

    F_M = svd(M)

    p = F_M.V[:,end]
    println(size(p))
    final_T = reshape(p, 3, 6)
    println(final_T)
    final_T = pre_tform_2D_lines \ final_T


    println("Solution is:")
    println(final_T)

    Ra, ta, Rb, tb = GetTransform(final_T)
    println("A:")
    println([Ra ta])
    println("B:")
    println([Rb tb])

    test_a = z_direction*dot(Ra[3,:], (center_3d' - closest_point - ta) / norm(center_3d' - closest_point - ta))
    test_b = z_direction*dot(Rb[3,:], (center_3d' - closest_point - tb) / norm(center_3d' - closest_point - tb))
    if test_a > 0 && test_b < 0
      final_R = Ra
      final_t = ta - closest_point;
    else
      final_R = Rb
      final_t = tb - closest_point;
    end


    stop = time_ns()
    time = stop-start;

    correct_T = ConstructPluckerTransformation(R,t)
    est_T = ConstructPluckerTransformation(final_R, final_t)
    correct_score = 0
    est_score = 0
    for i = 1:num_3d_points
     correct_p = correct_T*plucker_3d[:,i]
     est_p = est_T*plucker_3d[:,i]
     correct_score = correct_score + sum((norm_2d[1:2,i]./norm_2d[3,i] -correct_p[1:2]./correct_p[3]).^2)
     est_score = est_score + sum((norm_2d[1:2,i]./norm_2d[3,i] -est_p[1:2]./est_p[3]).^2)
    end
    println(correct_score)
    println(est_score)
    println(sum((M*p).^2))

    return final_R, -final_t, norm(final_R - final_T[1:3,1:3]), time/1e6, num_3d_points*num_2d_points, collect(1:num_3d_points)

end
