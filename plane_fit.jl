using JuMP, Juniper, GLPK, Cbc, Profile
const MOI = JuMP.MathOptInterface

function example_line( num_points = 100, inliers = 0.9)
    normal_model = [0, 0, 1]

    data = zeros(3,num_points)
    for i in 1:num_points
        if rand() < inliers
            data[:,i] = vcat(rand(2),[0.0])
        else
            data[:,i] = rand(3)
        end
    end

    # show(data)

    optimizer = Cbc.Optimizer
    # params = Dict{Symbol,Any}()
    # params[:nl_solver] = with_optimizer(Ipopt.Optimizer, print_level=0)
    params[:mip_solver] = with_optimizer(Cbc.Optimizer, logLevel=0)

    model = Model(with_optimizer(optimizer, params))
    @variable(model, normal[1:3])
    @variable(model, switch[1:num_points], Bin)
    @variable(model, alpha)
    @objective(model, Min, sum(switch)+400*alpha)
    @constraint(model, sum(normal) >= 1)
    @constraint(model, normal .<= 1)
    @constraint(model, alpha >= 0)
    @constraint(model, normal' * data .- (10000 .* switch') .<= alpha)
    @constraint(model, normal' * data .+(10000 .* switch') .>= -alpha)

    println("\nOptimizing")
    @time JuMP.optimize!(model)

    println("Solution is:")
    println(JuMP.value.(normal))
    # println(JuMP.value.(switch))
    println(JuMP.objective_value(model))
    println(JuMP.termination_status(model))

end

for points = 10:10:4000
  println(points)
  example_line(points, 0.9)
end

for pct_inliers = 0.70:0.05:0.95
  println(pct_inliers)
  example_line(4000, pct_inliers)
end

