using Gadfly, CSV, Cairo, Fontconfig

db = CSV.read("vgg_results_milp.csv")

p1 = plot(db, x="complexity", y="time", group="frame_id", color="frame_id", Geom.line)

draw(PNG("timing.png", 6inch, 4inch), p1)
