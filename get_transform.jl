using LinearAlgebra

include("skew.jl")

function GetTransform( P )

  P1 = P[:, 1:3];
  s = cbrt(1/det(P1))
  println(P1)
  println("Scale: ")
  println(s)
  Pscale = s * P
  P2scale = Pscale[:, 4:6]

  F = svd(P2scale)
  Z = [ 0 1 0; -1 0 0; 0 0 0]
  W = [ 0 -1 0; 1 0 0; 0 0 1]
  avgSGValue = (F.S[1]+F.S[2])/2

  da = det(F.U*W*F.Vt)
  db = det(F.U*W'*F.Vt)

  Ra = F.U*W*diagm(0=>[1; 1; da]) *F.Vt
  Rb = F.U*W'*diagm(0=>[1; 1; db]) *F.Vt

  tXa = avgSGValue * F.V * Z * F.Vt
  tXb = avgSGValue * F.V * Z' * F.Vt

  ta = SkewToVec(tXa)
  tb = SkewToVec(tXb)

  return Ra, ta, Rb, tb
end
