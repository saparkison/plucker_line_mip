using Plots

function outlier_complexity(x::Int)
  output = 2
  for i = 2:1:x
    output = output*(i+1)+1
  end
  return output
end

function redundant_complexity(x::Int)
  return (x+1)^(x+1)
end


pyplot()
xvec = 1:1:10
data = [factorial.(xvec) outlier_complexity.(xvec) redundant_complexity.(xvec)]
labels = ["No Outliers" "Outliers" "Many-to-One Associations"]

plot(xvec, data, size = (500,350), label = labels, yscale = :log10, xlabel = "Number of Lines", ylabel="Possible Associations",  title = "Complexity of the Association Problem")

savefig("results/complexity.png")
