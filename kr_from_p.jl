using LinearAlgebra

function KRfromP(P)

  N = size(P,1)
  H = P[:,1:N]

  S = H'
  F = LinearAlgebra.qr(S[end:-1:1,end:-1:1])
  Q = F.Q'
  R = Q[end:-1:1,end:-1:1]
  U = F.R'
  K = U[end:-1:1,end:-1:1]

  if det(R)<0
    K[:,1] = -K[:,1]
    R[1,:] = -R[1,:]
  end

  K = K / K[end,end]
  if K[1,1] < 0
    o = Array{Float64, 1}(undef, N)
    for i = 1:N
      if i <= 2
        o[i] = -1
      else
        o[i] = 1
      end
    end
    D = diagm(0 => o)
    K = K * D
    R = D * R
  end

  t = -P[:,1:N]\P[:,end]

  return K, R, t
end
