using Profile, LinearAlgebra, Statistics, Rotations, Gurobi, JuMP

include("plucker_lines.jl")
include("closest_point_to_lines.jl")
include("homogenous_lines.jl")
include("get_transform.jl")
include("fit_rotation.jl")
include("kr_from_p.jl")

function ConstructPluckerTransformation( R, t )
  skew_t = zeros(3,3)
  skew_t[3,2] = -t[1]
  skew_t[1,3] = -t[2]
  skew_t[2,1] = -t[3]

  skew_t = skew_t + skew_t'

  return hcat(R, skew_t*R)
end


function MipEst( line_3d_endpts, line_2d_endpts, K, R, t; rotation_constraint= :Linear, switch_constraint= :NoOutlier, z_direction = -1, image_height = 512, image_width = 512, normalize = false, offset = 40)

    num_3d_points = size(line_3d_endpts,1)
    num_2d_points = size(line_2d_endpts,1)

    center_3d = mean( [line_3d_endpts[:,1:3]; line_3d_endpts[:,4:6]], dims = 1)
    closest_point = ClosestPointToLines(line_3d_endpts)
    closest_point[3] = closest_point[3]  - z_direction*offset;
    #closest_point = zeros(3,1)
    if rotation_constraint == :SE2
      closest_point[2] = 0
    end
    shifted_line_3d = zeros(size(line_3d_endpts))
    shifted_line_3d[:,1:3] = line_3d_endpts[:,1:3] - ones(num_3d_points,1) * closest_point'
    shifted_line_3d[:,4:6] = line_3d_endpts[:,4:6] - ones(num_3d_points,1) * closest_point'
    mid_3d = (shifted_line_3d[:,1:3]+shifted_line_3d[:,4:6])./2
    plucker_3d = PluckerLines(shifted_line_3d)'

    norm_2d = HomogenousLines( line_2d_endpts, K)

    if normalize
      # Prenormalize 2D Lines
      shift_2d = mean( [norm_2d[1,:]./norm_2d[3,:] norm_2d[2,:]./norm_2d[3,:]], dims = 1)
      pre_shift_2D_lines = [1 0 -shift_2d[1]; 0 1 -shift_2d[2]; 0 0 1]
      lines_2D_shift = pre_shift_2D_lines * norm_2d[1:3,:]

      scale_2D = sqrt(2) / mean(((lines_2D_shift[1,:] ./ lines_2D_shift[3,:]).^2 + (lines_2D_shift[2,:] ./ lines_2D_shift[3,:]).^2).^0.5)
      pre_scale_2D_lines = diagm(0 => [scale_2D; scale_2D; 1])
      pre_tform_2D_lines = pre_scale_2D_lines * pre_shift_2D_lines
      norm_2d[1:3,:] = pre_tform_2D_lines * norm_2d[1:3,:]
      norm_2d[4:6,:] = pre_tform_2D_lines * norm_2d[4:6,:]
    end

    P_gt = [R VecToSkew( t)*R]
    for j = 1:num_3d_points
      for i = 1:num_2d_points
        proj = (1/2*K[1,1])*det(K)*pinv(K)'
        L = plucker_3d[:,j]
        l = proj*P_gt*L
        l = l/norm(proj[1:2,:])
        i_e = [line_2d_endpts[i,1:2]; [1]]
        i_e = i_e/norm(i_e)
        i_s = [line_2d_endpts[i,3:4]; [1]]
        i_s = i_s/norm(i_s)
        d1 = dot(i_e,l)
        d2 = dot(i_s,l)

        #@printf "i %d j %d %f %f\n" i j d1 d2
      end
    end


    #println("3d Pts")
    #println(plucker_3d)
    #println("2d Pts")
    #println(norm_2d)

    println(size(norm_2d))
    println(size(plucker_3d))
    println(num_3d_points)
    println(size(norm_2d[3,:]))
    println(K)
    println(center_3d)

    optimizer = Gurobi.Optimizer
    #model = Model(with_optimizer(optimizer, MIPFocus=3, Threads = 50, ImproveStartTime = 60))
    #model = Model(with_optimizer(optimizer, Presolve=2, MIPFocus=3, MIPGap = 0.00025, Threads = 50, ImproveStartTime = 60, BestObjStop=1e-4))
    model = Model(with_optimizer(optimizer, Threads = 50, OptimalityTol = 1e-9, MIPGap = 1e-9, TimeLimit = 2000))
    @variable(model, p[1:18])
    @variable(model, switch[1:num_3d_points, 1:num_2d_points], Bin)
    #for i = 1:num_3d_points
     # for j = 1:num_3d_points
     #   set_start_value(switch[i,j], initial_switch[i,j])
     # end
    #end
    #@objective(model, Min, (sum(switch)-(num_3d_points)*(num_2d_points-1))+sum(alpha))

    # p constraint
    #@variable(model, p_abs[1:18, 1:18] >= 0 )
    #@constraint(model, p_abs .>= p)
    #@constraint(model, p_abs .>= -p)
    #@constraint(model, sum(p_abs) == 1)
    bound = 1
    if rotation_constraint == :SE2
      bound = 1
    end
    @constraint(model, p[1:9] .<= 1)
    @constraint(model, p[1:9] .>= -1)
    @constraint(model, p[10:18] .<= bound)
    @constraint(model, p[10:18] .>= -bound)
    if rotation_constraint == :Fixed
      @constraint(model, p[1:3] .== R[:,1])
      @constraint(model, p[4:6] .== R[:,2])
      @constraint(model, p[7:9] .== R[:,3])
    elseif rotation_constraint == :Linear || rotation_constraint == :SE2
      @variable(model, R_abs[1:3, 1:3] >= 0 )
      @constraint(model, R_abs[:,1] .>= p[1:3])
      @constraint(model, R_abs[:,2] .>= p[4:6])
      @constraint(model, R_abs[:,3] .>= p[7:9])
      @constraint(model, R_abs[:,1] .>= -p[1:3])
      @constraint(model, R_abs[:,2] .>= -p[4:6])
      @constraint(model, R_abs[:,3] .>= -p[7:9])
      for i in 1:3
        @constraint(model, [1 1 1] * R_abs[:, i] .<= sqrt(3))
        @constraint(model, [1 1 1] * R_abs[i, :] .<= sqrt(3))
      end
      R_val = vcat([p[1], p[4], p[7]]', [p[2], p[5], p[8]]', [p[3], p[6], p[9]]')
      #@constraint(model, dot(R_val[3,:], center_3d)*z_direction >= 0.4)
      for i = 1:num_3d_points
        x = dot(R_val[1,:], mid_3d[i,:])
        y = dot(R_val[2,:], mid_3d[i,:])
        z = dot(R_val[3,:], mid_3d[i,:])
        for j = 1:num_2d_points
          @constraint(model, (z*z_direction) + 1000 * switch[i,j] >= 0.4 )
          #@constraint(model, x + 2.0 - 1000 * switch[i,j] <= image_width/(2*abs(K[1,1]))*(z*z_direction + 2))
          #@constraint(model, x - 2.0 + 1000 * switch[i,j] >= -image_width/(2*abs(K[1,1]))*(z*z_direction + 2))
          #@constraint(model, y + 2.0 - 1000 * switch[i,j] <= image_height/(2*abs(K[2,2]))*(z*z_direction +2))
          #@constraint(model, y - 2.0 + 1000 * switch[i,j] >= -image_height/(2*abs(K[2,2]))*(z*z_direction +2))
        end
      end
      if rotation_constraint == :SE2
        # rotation arround y axis
        @constraint(model, p[2] == 0);
        @constraint(model, p[4] == 0);
        #@constraint(model, p[5] == 1);
        @constraint(model, p[6] == 0);
        @constraint(model, p[8] == 0);
        # [t]_x R with y == 0
        @constraint(model, p[10] == 0);
        @constraint(model, p[12] == 0);
        @constraint(model, p[14] == 0);
        @constraint(model, p[16] == 0);
        @constraint(model, p[18] == 0);
      end
    end

    # switch constraints
    if switch_constraint == :NoOutlier
      for i = 1:num_3d_points
          @constraint(model, sum(switch[i,:]) == num_2d_points-1)
      end
      for i = 1:num_2d_points
          @constraint(model, sum(switch[:,i]) == num_3d_points-1)
      end
    elseif switch_constraint == :Outlier
      for i = 1:num_3d_points
          @constraint(model, sum(switch[i,:]) >= num_2d_points-1)
      end
      for i = 1:num_2d_points
          @constraint(model, sum(switch[:,i]) >= num_3d_points-1)
      end
    elseif switch_constraint == :Fixed
      initial_switch = ones(num_3d_points, num_2d_points) - Matrix(1.0I, num_3d_points, num_3d_points)
      @constraint(model, switch .== initial_switch)
    end


    # alpha constraint, slack variable
    @variable(model, α[1:num_3d_points, 1:num_2d_points, 1:2])
    @constraint(model, α .>= 0)
    println(size(norm_2d))
    println(size(plucker_3d))
    for j = 1:num_3d_points
      for i = 1:num_2d_points

        P = reshape(p, 3, 6)
        proj = (1/2)*det(K)*pinv(K)'
        L = plucker_3d[:,j]
        L = L/norm(L)
        l = proj*P*L
        l = l/norm(proj[1:2,:])

        i_e = [line_2d_endpts[i,1:2]; [1]]
        i_e = i_e/norm(i_e)
        i_s = [line_2d_endpts[i,3:4]; [1]]
        i_s = i_s/norm(i_s)
        @constraint(model, dot(i_e,l) .-(1e13*switch[j,i]) - α[j,i,1] .<= 0)
        @constraint(model, dot(i_e,l) .+(1e13*switch[j,i]) + α[j,i,1] .>= 0)
        @constraint(model, dot(i_s,l) .-(1e13*switch[j,i]) - α[j,i,2] .<= 0)
        @constraint(model, dot(i_s,l) .+(1e13*switch[j,i]) + α[j,i,2] .>= 0)

      end
    end

    lambda = 1e-6
    if(switch_constraint == :Outlier)
      @objective(model, Min, sum(α) + lambda*(sum(switch)))
    else
      @objective(model, Min, sum(α))
    end

    start = time_ns()

    println("\nOptimizing")
    JuMP.optimize!(model)

    stop = time_ns()
    time = stop-start;

    p_final = JuMP.value.(p)
    println(size(p_final))
    final_T = reshape(p_final, 3, 6)
    if normalize
      final_T = pre_tform_2D_lines \ final_T
    end

    println("Solution is:")
    println(final_T)
    println(JuMP.value.(α))
    final_switch = JuMP.value.(switch)
    println(sum(final_switch.<=1e-4))
    associations = zeros(Int, num_3d_points, 1)
    for i = 1:num_3d_points
      for j = 1:num_2d_points
        if final_switch[i,j] <= 1e-5
          associations[i] = j
        end
      end
    end
    println(associations)

    s = cbrt(1/det(final_T[1:3,1:3]))
    println(s*final_T[1:3, 1:3])
    final_R = FitRotation(final_T[1:3, 1:3])
    println(final_R)

    final_skew_t = final_R'*(s*final_T[1:3,4:6])
    println(SkewToVec( final_skew_t ))
    println(closest_point)
    #final_t = SkewToVec( s*final_skew_t );
    final_t = -SkewToVec( final_skew_t ) + closest_point;

    println(final_R'*R)

    return final_R, final_t, norm(final_R - final_T[1:3,1:3]), time/1e6, num_3d_points*num_2d_points, associations

end
