using Profile, CSV, LinearAlgebra, Statistics, Rotations, DataFrames, Printf, Gurobi, JuMP, StatsBase

include("plucker_lines.jl")
include("closest_point_to_lines.jl")
include("homogenous_lines.jl")
include("get_transform.jl")
include("kr_from_p.jl")

function ConstructPluckerTransformation( R, t )
  skew_t = zeros(3,3)
  skew_t[3,2] = -t[1]
  skew_t[1,3] = -t[2]
  skew_t[2,1] = -t[3]

  skew_t = skew_t + skew_t'

  return hcat(R, R*skew_t)
end


function LinePoseEst( line_3d_endpts, line_2d_endpts, K, R, t )

  num_3d_points = size(line_3d_endpts,1)
  num_2d_points = size(line_2d_endpts,1)

  center_3d = mean( [line_3d_endpts[:,1:3]; line_3d_endpts[:,4:6]], dims = 1)
  closest_point = ClosestPointToLines(line_3d_endpts)
  shifted_line_3d = zeros(size(line_3d_endpts))
  shifted_line_3d[:,1:3] = line_3d_endpts[:,1:3] - ones(num_3d_points,1) * closest_point'
  shifted_line_3d[:,4:6] = line_3d_endpts[:,4:6] - ones(num_3d_points,1) * closest_point'
  plucker_3d = PluckerLines(shifted_line_3d)'

  norm_2d = HomogenousLines( line_2d_endpts, K)

  flatened_2d = hcat(norm_2d[1:3,:], norm_2d[4:6,:])

  # Prenormalize 2D Lines
  shift_2d = mean( [flatened_2d[1,:]./flatened_2d[3,:] flatened_2d[2,:]./flatened_2d[3,:]], dims = 1)
  pre_shift_2D_lines = [1 0 -shift_2d[1]; 0 1 -shift_2d[2]; 0 0 1]
  lines_2D_shift = pre_shift_2D_lines * flatened_2d[1:3,:]

  scale_2D = sqrt(2) / mean(((lines_2D_shift[1,:] ./ lines_2D_shift[3,:]).^2 + (lines_2D_shift[2,:] ./ lines_2D_shift[3,:]).^2).^0.5)
  pre_scale_2D_lines = diagm(0 => [scale_2D; scale_2D; 1])
  pre_tform_2D_lines = pre_scale_2D_lines * pre_shift_2D_lines
  println(pre_tform_2D_lines)

  norm_2d[1:3,:] = pre_tform_2D_lines * norm_2d[1:3,:]
  norm_2d[4:6,:] = pre_tform_2D_lines * norm_2d[4:6,:]

  #println("3d Pts")
  #println(plucker_3d)
  #println("2d Pts")
  #println(norm_2d)

  three_d_similarity = zeros(num_3d_points, num_3d_points)
  for i = 1:num_3d_points
    for j = 1:num_3d_points
      three_d_similarity[i,j] = abs(dot(plucker_3d[4:6,i],plucker_3d[4:6,j])/(norm(plucker_3d[4:6,i])*norm(plucker_3d[4:6])))
    end
  end

  two_d_similarity = zeros(num_2d_points, num_2d_points)
  for i = 1:num_2d_points
    for j = 1:num_2d_points
      two_d_similarity[i,j] = abs(dot(norm_2d[4:6,i],norm_2d[4:6,j])/(norm(norm_2d[4:6,i])*norm(norm_2d[4:6])))
    end
  end
  start = time_ns()
  optimizer = Gurobi.Optimizer
  model = Model(with_optimizer(optimizer, Threads = 50))
  @variable(model, alpha[1:num_3d_points,1:num_3d_points])
  @variable(model, switch[1:num_3d_points, 1:num_2d_points], Bin)
  @objective(model, Min, sum(alpha))

  # switch constraints
  for i = 1:num_3d_points
      @constraint(model, sum(switch[i,:]) == num_2d_points-1)
  end
  for i = 1:num_2d_points
      @constraint(model, sum(switch[:,i]) == num_3d_points-1)
  end


  # alpha constraint, slack variable
  @constraint(model, alpha .>= 0)
  for i = 1:num_2d_points
    for j = 1:num_3d_points
      for new_i = 1:num_2d_points
        for new_j = 1:num_2d_points
          @constraint(model, two_d_similarity[new_i,new_j] -(1000*(switch[i,new_i]+switch[j,new_j]))<= three_d_similarity[i,j]+alpha[i,j])
          @constraint(model, two_d_similarity[new_i,new_j] +(1000*(switch[i,new_i]+switch[j,new_j]))>= -three_d_similarity[i,j]-alpha[i,j])
        end
      end
    end
  end

  println("\nOptimizing")
  JuMP.optimize!(model)
  stop = time_ns()
  println("Elaped time")
  println((stop-start)/1e6)

  final_switch = JuMP.value.(switch)
  associations = zeros(data_size, 1)
  for i = 1:data_size
    for j = 1:data_size
      if final_switch[i,j] < 1e-4
        associations[j] = i
      end
    end
  end
  println(associations)

  return (stop-start)/1e6, num_3d_points - sum(associations.==collect(1:num_3d_points))

end

lines_3d = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.l3d"; header=false, delim=' ', ignorerepeated=true))
associations = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.nview-lines"; header=false, delim=' ', ignorerepeated=true, missingstring="*"))

df = DataFrame(time_ms=Float64[], num_points=Float64[], error=Int[])

for num = 15:5:30
  for frame = 0:1:10
    for itt = 1:10
      string_num = @sprintf "%03d" frame
      p_file = string("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.", string_num, ".P")
      P = Matrix(CSV.read(p_file; header=false, delim =' ', ignorerepeated=true))
      K, R, t = KRfromP(P)
      println(K)
      lines_file = string("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.", string_num, ".lines")
      lines_2d = Matrix(CSV.read(lines_file; header=false, delim=' ', ignorerepeated=true))
      perm = sample(1:size(lines_3d,1), num, replace = false)
      time, error = LinePoseEst( lines_3d[perm,:], lines_2d[associations[perm,frame+1].+1,:], K, R, t)
      push!(df,[time, num, error])
      CSV.write("perm_3d_2d_results.csv",df)
      println("GT vs Estim")
    end
  end
end

p1 = plot(df, x="num", y="time_ms", Geom.boxplot(suppress_outliers=true))
draw(PNG("perm_3d_2d_results.png", 8inch, 8inch),p1)

