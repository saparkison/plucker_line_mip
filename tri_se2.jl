using CSV, DataFrames, Printf, StatsBase, Rotations, LinearAlgebra, Images, Parsers

include("mip_est.jl")
include("pribyl_et_al.jl")
include("results_figure.jl")

function stringToTransform(rph_xyz_string::String)
  println(rph_xyz_string)
  m = match(r"rotate_deg=([0-9.+-]+),([0-9.+-]+),([0-9.+-]+)", rph_xyz_string)
  r = Parsers.parse(Float64, m.captures[1])
  println(r)
  p = Parsers.parse(Float64, m.captures[2])
  println(p)
  h = Parsers.parse(Float64, m.captures[3])
  println(h)

  m = match(r"translate_m=([0-9.+-]+),([0-9.+-]+),([0-9.+-]+)", rph_xyz_string)
  x = Parsers.parse(Float64, m.captures[1])
  println(x)
  y = Parsers.parse(Float64, m.captures[2])
  println(y)
  z = Parsers.parse(Float64, m.captures[3])
  println(z)

  t = [x y z]
  R = RotZ(h*pi/180) * RotY(p*pi/180) * RotX(r*pi/180)

  return R, t'
end

function filter_3D( line_endpts_3D )


  keep = BitArray(undef, size(line_endpts_3D, 1))
  keep .= true

  for i = 1:size(line_endpts_3D,1)-1
    current_s = line_endpts_3D[i, 1:3]
    current_e = line_endpts_3D[i, 4:6]
    if current_e[3] < 2.0 || current_s[3] < 2.0
      keep[i] = false
      continue
    end
    start = i+1
    for j = start:size(line_endpts_3D,1)
      querry_s = line_endpts_3D[j, 1:3]
      querry_e = line_endpts_3D[j, 4:6]
      if (norm(current_e - querry_e) < 1.5 || norm(current_e - querry_s) < 1.5 ||
          norm(current_s - querry_s) < 1.5 || norm(current_s - querry_e) < 1.5)
        keep[i] = false
      end
    end
  end

  return line_endpts_3D[keep,:]
end

base_string = string("/home/stevenparkison/datasets/tri/sparkison-data-mapping/data_20190909/")

p_file = string(base_string, "camera_matrix.txt")
P = Matrix(CSV.read(p_file; header=false, delim =' ', ignorerepeated=true))
println(P)
K, R_CV, t_CV = KRfromP(P)
t_CV = -R_CV*t_CV
println(K)
println(R_CV)
println(t_CV)

df = DataFrame(time_ms = Float64[], rad_error=Float64[], m_error=Float64[], frame_id=Int[], constraint_method=String[])

#for frame in [350 380 390]
for frame in 350:1:390
  string_num = @sprintf "%d" frame
  test_lines = readlines(string(base_string, string_num,"_3d_lines.txt"))
  R_GV, t_GV = stringToTransform(test_lines[1])
  R_VG = R_GV'
  t_VG = -R_GV'*t_GV
  R_CG = R_CV*R_VG
  t_CG = R_CV*t_VG+t_CV
  println(R_CG)
  println(t_CG)
  lines_3d = Matrix(CSV.read(string(base_string,string_num,"_3d_lines.txt"); header=false, delim=' ', ignorerepeated=true, datarow=2))
  angle = acos(dot(R_CG'*[1, 0 , 0], [1, 0, 0]))
  axis = cross(R_CG'*[1, 0 , 0], [1, 0, 0])
  #R_mod = AngleAxis(angle, axis[1], axis[2], axis[3])'
  #t_mod = -R_CG[:,1]*t_CG[1]
  R_mod = R_CG'
  t_mod = -R_CG'*t_CG
  new_R = R_CG*R_mod
  new_t = R_CG*t_mod+t_CG
  new_lines_3d = ( vcat(hcat(R_mod', zeros(3,3)), hcat(zeros(3,3), R_mod')) * lines_3d' + vcat(-R_mod'*t_mod, -R_mod'*t_mod) * ones(1,size(lines_3d,1)))'
  new_lines_3d = filter_3D(new_lines_3d)
  println("New transformations")
  println(new_R)
  println(new_t)

  lines_file = string(base_string, string_num, "_semseg_lines.txt")
  lines_2d = Matrix(CSV.read(lines_file; header=false, delim=' ', ignorerepeated=true))
  new_lines_2d = zeros(size(lines_2d, 1),size(lines_2d,2))
  new_lines_2d[:,1:2:3] = lines_2d[:,2:2:4]
  new_lines_2d[:,2:2:4] = lines_2d[:,1:2:3]
  image_file = string(base_string, string_num, ".png")
  image = load(image_file)
  draw_results_figure(image, lines_2d, string("results/tri/2d", string_num, ".jpg"))

  a = zeros(Int, size(new_lines_3d,1))
  if frame == 350 
    a[2] = 22
    a[3] = 1
    a[7] = 18
    a[9] = 30
    a[12] = 11
    a[17] = 14
    a[21] = 21
    a[32] = 19
  elseif frame == 380
    a[2] = 12
    a[4] = 11
    a[12] = 33
    a[14] = 9
    a[25] = 21
    a[26] = 32
    a[30] = 6
    a[32] = 26
  elseif frame == 390
    a[4] = 13
    a[6] = 2
    a[7] = 20
    a[9] = 27
    a[10] = 16
    a[13] = 29
    a[15] = 31
    a[17] = 4
    a[28] = 24
    a[29] =15
  end
  not_zero = .!isequal.(a, 0)
  #new_lines_3d = new_lines_3d[end-size(lines_2d,1)+1:end,:]
  # Linear
  #est_R_l, est_t_l, norm_r, time, complexity, a_l= MipEst( new_lines_3d[not_zero,:], lines_2d[a[not_zero],:], K, new_R, new_t;
  est_R_l, est_t_l, norm_r, time, complexity, a_l= MipEst( new_lines_3d, lines_2d, K, new_R, new_t;
                                                    rotation_constraint = :SE2,
                                                    switch_constraint=:Outlier,
                                                    z_direction = 1,
                                                    image_height = 1216,
                                                    image_width = 1936,
                                                    offset = 25)


  #indices = .!isequal.(a, 0)
  #println(size(indices))
  #est_R, est_t, norm_r, time, complexity, a_prib= PribylEtAl( new_lines_3d[indices[:],:], lines_2d[a[indices],:], K, new_R, new_t; z_direction = 1)
  #est_t = [-est_t[1] -est_t[3] -est_t[2]]'
  #println(est_R)
  #mod = [1 0 0; 0 1 0; 0 0 -1]
  #est_t = mod*est_t
  #est_R = mod*est_R
  #est_R[2:3, 2:3] = [0 -1; -1 0] * est_R[2:3, 2:3]
  #println(est_R)
  push!(df,[time, AngleAxis((est_R_l)'*new_R).theta, norm(new_t-est_t_l), frame, "Fixed Switch"])
  CSV.write("results/tri/results.csv",df)
  println("GT vs Estim")
  println(hcat(new_R,new_t))
  #println(hcat(est_R,est_t))
  println(hcat(est_R_l,est_t_l))
  #indices = .!isequal.(a, -1)
  #println(size(indices))
  #a .=0
  #a = zeros(size(lines_3d,1))

 # for i = 1:size(new_lines_3d,1)
    draw_results_figure_3d(image, lines_2d, new_lines_3d, a, K, est_R_l, new_t, string("results/tri/mip_frame", string_num,".jpg"); height= 1216, width=1936, z_direction = 1)
  #end
end
