using CSV, DataFrames, Printf, StatsBase, Rotations, LinearAlgebra

include("mip_est.jl")
include("results_figure.jl")
include("pribyl_et_al.jl")

lines_3d = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.l3d"; header=false, delim=' ', ignorerepeated=true))
associations = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.nview-lines"; header=false, delim=' ', ignorerepeated=true, missingstring="*"))

df = DataFrame(time_ms = Float64[], num_points= Int[], rad_error=Float64[], m_error=Float64[], association_error = Int[], frame_id=Int[], constraint_method=String[])

indices_matrix = zeros(Int, 10, 69)
for itteration = 1:1:10
  indices_matrix[itteration,:] = sample(1:69, 69, replace = false)
end

for frame = 0:1:0
  indices_matrix = zeros(Int, 20, 20)
  not_missing = .!isequal.(associations[:,frame+1], missing)
  possible_indices = collect(1:69)[not_missing]
  for itteration = 1:1:20
    indices_matrix[itteration,:] = sample(possible_indices, 20, replace = false)
  end


  string_num = @sprintf "%03d" frame
  p_file = string("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.", string_num, ".P")
  P = Matrix(CSV.read(p_file; header=false, delim =' ', ignorerepeated=true))
  K, R, t = KRfromP(P)
  R = R
  println(K)
  lines_file = string("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.", string_num, ".lines")
  lines_2d = Matrix(CSV.read(lines_file; header=false, delim=' ', ignorerepeated=true))
  image_file = string("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.", string_num, ".pgm")
  image = load(image_file)
  draw_results_figure(image, lines_2d, string("results/2d", string_num, ".jpg"))
  for num_lines = 10:2:20
    for itteration = 1:1:20
      indices = indices_matrix[itteration, 1:num_lines]
      est_R, est_t, norm_r, time, complexity, a= PribylEtAl( lines_3d[indices,:], lines_2d[associations[indices,frame+1].+1,:], K, R, t)
      push!(df,[time, num_lines, AngleAxis((est_R)'*R).theta, norm(t-est_t), num_lines-sum(a.==collect(1:num_lines)), frame, "Pribyl"])
      CSV.write("results/vgg/sampled/results.csv",df)
      println("GT vs Estim")
      println(hcat(R,t))
      println(hcat(est_R,est_t))
      string_it = @sprintf "%03d" itteration
      string_lines = @sprintf "%03d" num_lines
      draw_results_figure_3d(image, lines_2d[associations[indices, frame+1].+1,:], lines_3d[indices,:], a, K, est_R, est_R*-est_t, string("results/vgg/sampled/pribyl_lines", string_lines, "it", string_it, "frame", string_num, ".jpg"))
    end
  end
  for num_lines = 10:2:20
    for itteration = 1:1:20
      indices = indices_matrix[itteration, 1:num_lines]
      est_R, est_t, norm_r, time, complexity, a= MipEst( lines_3d[indices,:], lines_2d[associations[indices,frame+1].+1,:], K, R, t; switch_constraint = :Fixed)
      est_t = est_t
      push!(df,[time, num_lines, AngleAxis((est_R)'*R).theta, norm(t-est_t), num_lines-sum(a.==collect(1:num_lines)), frame, "Known Associations"])
      CSV.write("results/vgg/sampled/results.csv",df)
      println("GT vs Estim")
      println(hcat(R,t))
      println(hcat(est_R,est_t))
      string_it = @sprintf "%03d" itteration
      string_lines = @sprintf "%03d" num_lines
      draw_results_figure_3d(image, lines_2d[associations[indices, frame+1].+1,:], lines_3d[indices,:], a, K, est_R, est_t, string("results/vgg/sampled/fixed_switch_lines", string_lines, "it", string_it, "frame", string_num, ".jpg"))
    end
  end
  for num_lines = 10:2:9
    for itteration = 1:1:20
      indices = indices_matrix[itteration, 1:num_lines]
      est_R, est_t, norm_r, time, complexity, a= MipEst( lines_3d[indices,:], lines_2d[associations[indices,frame+1].+1,:], K, R, t)
      est_t = est_t
      push!(df,[time, num_lines, AngleAxis((est_R)'*R).theta, norm(t-est_t), num_lines-sum(a.==collect(1:num_lines)), frame, "SE3"])
      CSV.write("vgg_results_sampled.csv",df)
      CSV.write("results/vgg/sampled/results.csv",df)
      println("GT vs Estim")
      println(hcat(R,t))
      println(hcat(est_R,est_t))
      string_it = @sprintf "%03d" itteration
      string_lines = @sprintf "%03d" num_lines
      draw_results_figure_3d(image, lines_2d[associations[indices, frame+1].+1,:], lines_3d[indices,:], a, K, -est_R, est_R*est_t, string("results/vgg/sampled/linear_lines", string_lines, "it", string_it, "frame", string_num, ".jpg"))
    end
  end
  for num_lines = 10:2:20
    for itteration = 1:1:20
      indices = indices_matrix[itteration, 1:num_lines]
      # Make SE(2)
      angle = acos(dot(R'*[0, 1 , 0], [0, 1, 0]))
      axis = cross(R'*[0, 1 , 0], [0, 1, 0])
      R_mod = AngleAxis(angle, axis[1], axis[2], axis[3])'
      t_mod = -R[:,2]*t[2]
      new_R = R*R_mod
      new_t = R*t_mod+t
      new_lines_3d = ( vcat(hcat(R_mod', zeros(3,3)), hcat(zeros(3,3), R_mod')) * lines_3d' + vcat(-R_mod'*t_mod, -R_mod'*t_mod) * ones(1,size(lines_3d,1)))'
      println("New transformations")
      println(new_R)
      println(new_t)
      est_R, est_t, norm_r, time, complexity, a= MipEst( new_lines_3d[indices,:], lines_2d[associations[indices,frame+1].+1,:], K, R, t; rotation_constraint = :Linear, switch_constraint = :NoOutlier, offset = 60)
      est_t = est_t
      push!(df,[time, num_lines, AngleAxis(est_R'*new_R).theta, norm(new_t-est_t), num_lines-sum(a.==collect(1:num_lines)), frame, "SE2"])
      CSV.write("results/vgg/sampled/results.csv",df)
      println("GT vs Estim")
      println(hcat(new_R,new_t))
      println(hcat(est_R,est_t))
      string_it = @sprintf "%03d" itteration
      string_lines = @sprintf "%03d" num_lines
      draw_results_figure_3d(image, lines_2d[associations[indices, frame+1].+1,:], new_lines_3d[indices,:], a, K, est_R, est_t, string("results/vgg/sampled/fixed_rotationslines", string_lines, "it", string_it, "frame", string_num, ".jpg"))
    end
  end
end
