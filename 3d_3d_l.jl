using JuMP, Juniper, Gurobi, Profile, CSV, LinearAlgebra, Rotations, DataFrames, Gadfly, Cairo, Fontconfig, StatsBase
const MOI = JuMP.MathOptInterface

include("fit_rotation.jl")
include("plucker_lines.jl")
include("skew.jl")

function example_line( filename, data_size::Int, noise::Float64, num_outlier::Int = 0 )

    df = CSV.read(filename; header=false, delim=' ', ignorerepeated=true)
    data = Matrix(df)
    indices = sample(1:size(df,1), data_size, replace=false)
    data = data[indices,:]

    R_gt = rand(RotMatrix{3})
    t_gt = rand(3)
    perm = sample(1:data_size, data_size, replace = false)
    new_data = data[perm, :] +randn(size(data,1), size(data,2))*noise
    new_data = vcat(new_data, rand(num_outlier,6))
    for i = 1:size(new_data,1)
      new_data[i,1:3]= (R_gt*new_data[i, 1:3]+t_gt)'
      new_data[i,4:6]= (R_gt*new_data[i, 4:6]+t_gt)'
    end

    data = PluckerLines(data)'
    new_data = PluckerLines(new_data)'

    show(data)


    println()
    show(R_gt)
    println()
    show(t_gt)

    num_points = size(data,2)
    num_points_new = size(new_data,2)



    model = Model(with_optimizer(Gurobi.Optimizer, Threads = 50, ImproveStartTime=30))
    @variable(model, estimated_T[1:6,1:6])
    @variable(model, switch[1:num_points, 1:num_points_new], Bin)
    @variable(model, alpha[1:num_points, 1:num_points_new])
    @objective(model, Min, sum(switch)+sum(alpha))
    # T constraints
    @constraint(model, estimated_T[4:6, 1:3] .== 0)
    @constraint(model, estimated_T[1:3, 1:3] .== estimated_T[4:6,4:6])
    @constraint(model, estimated_T[1:3, 1:3] .<= 1)
    @constraint(model, estimated_T[1:3, 1:3] .>= -1)
    @variable(model, R_abs[1:3, 1:3] >= 0 )
    @constraint(model, R_abs .>= estimated_T[1:3, 1:3])
    @constraint(model, R_abs .>= -estimated_T[1:3, 1:3])
    for i in 1:3
      @constraint(model, [1 1 1] * R_abs[:, i] .<= sqrt(3))
      @constraint(model, [1 1 1] * R_abs[i, :] .<= sqrt(3))
    end

    #@constraint(model, estimated_T[1:3, 1:3] .== 0)
    #@constraint(model, T[1, 4] .== 0)
    #@constraint(model, T[2, 5] .== 0)
    #@constraint(model, T[3, 6] .== 0)
    #@constraint(model, T[2, 4] .== -T[1,5])
    #@constraint(model, T[3, 4] .== -T[1,6])
    #@constraint(model, T[3, 5] .== -T[2,6])

    # switch constraints
    for i = 1:num_points
        @constraint(model, sum(switch[i,:]) >= num_points_new-1)
    end
    for i = 1:num_points_new
        @constraint(model, sum(switch[:,i]) >= num_points-1)
    end

    # alpha constraint
    @constraint(model, alpha .>= 0)

    # cost function constraint
    for i = 1:num_points
        for j = 1:num_points_new
          @constraint(model, new_data[:,j]-estimated_T*data[:,i] .- (1000 * switch[i,j]) .<= alpha[i,j])
          @constraint(model, new_data[:,j]-estimated_T*data[:,i] .+ (1000 * switch[i,j]) .>= -alpha[i,j])
          #@constraint(model, new_data[:,i]*(estimated_T*data[:,j])'-(estimated_T*data[:,j])*new_data[:,i]'.- (1000 * switch[i,j]) .<= alpha[i, j])
          #@constraint(model, new_data[:,i]*(estimated_T*data[:,j])'-(estimated_T*data[:,j])*new_data[:,i]'.+ (1000 * switch[i,j]) .>= -alpha[i, j])
        end
    end

    println("\nOptimizing")
    start = time_ns()
    JuMP.optimize!(model)
    stop = time_ns()
    time = stop-start;
    println("Elaped time")
    println((stop-start)/1e6)

    final_switch = JuMP.value.(switch)
    associations = zeros(data_size, 1)
    for i = 1:data_size
      for j = 1:data_size
        if final_switch[i,j] < 1e-4
          associations[j] = i
        end
      end
    end

    println(JuMP.value.(alpha))
    # println(JuMP.value.(switch))
    println(JuMP.objective_value(model))
    println(JuMP.termination_status(model))

    final_T = JuMP.value.(estimated_T)
    final_R = FitRotation(final_T[1:3, 1:3])

    final_skew_t = final_T[1:3,4:6]*final_R'
    final_t = SkewToVec( final_skew_t )

    println(final_R)
    println(final_t)

    println(perm)
    println(associations)

    return time/1e6, AngleAxis(final_R'*R_gt).theta, norm(final_t-t_gt), num_points - sum(associations==perm)

end

df = DataFrame(time_ms = Float64[], num_points= Int[], rad_error=Float64[], m_error=Float64[], association_error = Int[])

for num = 10:2:20
  for i = 1:10
    time, rad_e, m_e, a_e = example_line("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.l3d", num, 1e-3)
    push!(df, [time, num, rad_e, m_e, a_e])
    CSV.write("line_size_results.csv", df)
  end
end

p1 = plot(df, x="num_points", y="time_ms", Geom.boxplot(suppress_outliers=true))
p2 = plot(df, x="num_points", y="rad_error", Geom.boxplot(suppress_outliers=true))
p3 = plot(df, x="num_points", y="m_error", Geom.boxplot(suppress_outliers=true))
p4 = plot(df, x="num_points", y="association_error", Geom.boxplot(suppress_outliers=true))
draw(PNG("line_size_results.png", 8inch, 8inch),vstack(hstack(p1,p2),hstack(p3,p4)))

df = DataFrame(time_ms = Float64[], num_outliers= Int[], rad_error=Float64[], m_error=Float64[], association_error = Int[])

for num = 2:2:10
  for i = 1:10
    time, rad_e, m_e, a_e = example_line("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.l3d", 15, 1e-3, num)
    push!(df, [time, num, rad_e, m_e, a_e])
    CSV.write("line_outlier_results.csv", df)
  end
end

p1 = plot(df, x="num_outliers", y="time_ms", Geom.boxplot(suppress_outliers=true))
p2 = plot(df, x="num_outliers", y="rad_error", Geom.boxplot(suppress_outliers=true))
p3 = plot(df, x="num_outliers", y="m_error", Geom.boxplot(suppress_outliers=true))
p4 = plot(df, x="num_outliers", y="association_error", Geom.boxplot(suppress_outliers=true))
draw(PNG("line_outlier_results.png", 8inch, 8inch),vstack(hstack(p1,p2),hstack(p3,p4)))

