using LinearAlgebra

function FitRotation( M )

  F = svd(M+Matrix{Float64}(I,3,3)*1e-6)
  d = det(F.U*F.Vt)
  diag = Matrix{Float64}(I,3,3)
  diag[3,3] =d

  return F.U*diag*F.Vt
end
