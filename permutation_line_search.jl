using JuMP, Juniper, Gurobi, Profile, CSV, LinearAlgebra, Rotations, DataFrames, Gadfly, Cairo, Fontconfig, StatsBase

include("plucker_lines.jl")

function perm_search(data_size::Int, noise::Float64, num_outlier::Int = 0)

  df = CSV.read("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.l3d"; header=false, delim=' ', ignorerepeated=true)
  original_data = Matrix(df)
  indices = sample(1:size(df,1), data_size + num_outlier, replace=false)
  data = original_data[indices[1:data_size], :]

  plucker_data = PluckerLines(data)'
  similarity_matrix = zeros(data_size,data_size)
  for i = 1:data_size
    for j = 1:data_size
      #similarity_matrix[i,j] = norm(plucker_data[:,i]-plucker_data[:,j])
      similarity_matrix[i,j] = abs(dot(plucker_data[4:6,i],plucker_data[4:6,j]))
    end
  end

  new_data_size = data_size + num_outlier
  new_data = original_data[indices, :] + rand(new_data_size, 6) * noise

  plucker_new_data = PluckerLines(new_data)'
  new_similarity_matrix = zeros(new_data_size,new_data_size)
  for i = 1:new_data_size
    for j = 1:new_data_size
      #new_similarity_matrix[i,j] = norm(plucker_new_data[:,i]-plucker_new_data[:,j])
      new_similarity_matrix[i,j] = abs(dot(plucker_new_data[4:6,i],plucker_new_data[4:6,j]))
    end
  end

  start = time_ns()
  optimizer = Gurobi.Optimizer
  model = Model(with_optimizer(optimizer, Threads = 50))
  @variable(model, alpha[1:data_size,1:data_size])
  @variable(model, switch[1:data_size, 1:new_data_size], Bin)
  @objective(model, Min, sum(alpha))

  # switch constraints
  for i = 1:data_size
      @constraint(model, sum(switch[i,:]) == new_data_size-1)
  end
  for i = 1:new_data_size
      @constraint(model, sum(switch[:,i]) >= data_size-1)
  end


  # alpha constraint, slack variable
  @constraint(model, alpha .>= 0)
  for i = 1:data_size
    for j = 1:data_size
      for new_i = 1:new_data_size
        for new_j = 1:new_data_size
          @constraint(model, similarity_matrix[i,j]-new_similarity_matrix[new_i,new_j] -(1000*(switch[i,new_i]+switch[j,new_j]))<= alpha[i,j])
          @constraint(model, similarity_matrix[i,j]-new_similarity_matrix[new_i,new_j] +(1000*(switch[i,new_i]+switch[j,new_j]))>= -alpha[i,j])
        end
      end
    end
  end

  println("\nOptimizing")
  JuMP.optimize!(model)
  stop = time_ns()
  println("Elaped time")
  println((stop-start)/1e6)

  final_switch = JuMP.value.(switch)
  associations = zeros(data_size, 1)
  for i = 1:data_size
    for j = 1:data_size
      if final_switch[i,j] < 1e-4
        associations[j] = i
      end
    end
  end
  println(associations)


  return (stop-start)/1e6, data_size - sum(associations.==collect(1:data_size))
end

df = DataFrame(time_ms = Float64[], num_outliers= Int[], errors = Int[])
for num = 5:5:40
  for i = 1:10
    time, error = perm_search(15, 1e-4, num)
    push!(df, [time, num, error])
    CSV.write("perm_outlier_results.csv", df)
  end
end

p = plot(df, x="num_outliers", y="time_ms", Geom.boxplot(suppress_outliers=false))
draw(PNG("perm_outliers_results.png", 8inch, 8inch),p)

df = DataFrame(time_ms = Float64[], num_points= Int[], error=Float64[])

for num = 10:5:50
  for i = 1:20
    time, error = perm_search(num, 1e-4)
    push!(df, [time, num, error])
    CSV.write("perm_size_results.csv", df)
  end
end


p1 = plot(df, x="num_points", y="time_ms", Geom.boxplot(suppress_outliers=false))
draw(PNG("perm_results.png", 8inch, 8inch),p1)


