function SkewToVec( noisy_mat )
  ss_mat = (noisy_mat - noisy_mat')/2
  return [ss_mat[3,2]; ss_mat[1,3]; ss_mat[2,1]]
end

function VecToSkew( vec )
  skew = zeros(3,3)
  skew[3,2] = vec[1]
  skew[1,3] = vec[2]
  skew[2,1] = vec[3]

  skew = skew - skew'
  return skew
end
