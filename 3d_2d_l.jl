using JuMP, Juniper, Printf, Gurobi, Profile, CSV, LinearAlgebra, Rotations, DataFrames, Gadfly, Cairo, Fontconfig, StatsBase
const MOI = JuMP.MathOptInterface

include("plucker_lines.jl")
include("closest_point_to_lines.jl")
include("homogenous_lines.jl")
include("fit_rotation.jl")
include("kr_from_p.jl")
include("skew.jl")

function LinePoseEst( line_3d_endpts, line_2d_endpts, K )

    num_3d_points = size(line_3d_endpts,1)
    num_2d_points = size(line_2d_endpts,1)

    center_3d = mean( [line_3d_endpts[:,1:3]; line_3d_endpts[:,4:6]], dims = 1)
    closest_point = ClosestPointToLines(line_3d_endpts)
    shifted_line_3d = zeros(size(line_3d_endpts))
    shifted_line_3d[:,1:3] = line_3d_endpts[:,1:3] - ones(num_3d_points,1) * closest_point'
    shifted_line_3d[:,4:6] = line_3d_endpts[:,4:6] - ones(num_3d_points,1) * closest_point'
    plucker_3d = PluckerLines(shifted_line_3d)'

    norm_2d = HomogenousLines( line_2d_endpts, K)

    flatened_2d = hcat(norm_2d[1:3,:], norm_2d[4:6,:])

    # Prenormalize 2D Lines
    shift_2d = mean( [flatened_2d[1,:]./flatened_2d[3,:] flatened_2d[2,:]./flatened_2d[3,:]], dims = 1)
    pre_shift_2D_lines = [1 0 -shift_2d[1]; 0 1 -shift_2d[2]; 0 0 1]
    lines_2D_shift = pre_shift_2D_lines * flatened_2d[1:3,:]

    scale_2D = sqrt(2) / mean(((lines_2D_shift[1,:] ./ lines_2D_shift[3,:]).^2 + (lines_2D_shift[2,:] ./ lines_2D_shift[3,:]).^2).^0.5)
    pre_scale_2D_lines = diagm(0 => [scale_2D; scale_2D; 1])
    pre_tform_2D_lines = pre_scale_2D_lines * pre_shift_2D_lines
    println(pre_tform_2D_lines)

    norm_2d[1:3,:] = pre_tform_2D_lines * norm_2d[1:3,:]
    norm_2d[4:6,:] = pre_tform_2D_lines * norm_2d[4:6,:]


    #xy_2d = vcat(line_2d_endpts[:,1:2]', ones(1,num_2d_points), line_2d_endpts[:,3:4]', ones(1,num_2d_points))
    #P = hcat(inv(K)'.*det(K), K)

    model = Model(with_optimizer(Gurobi.Optimizer, Threads = 50, ImproveStartTime=30))
    # T constraints
    @variable(model, estimated_T[1:3, 1:6])
    #@constraint(model, estimated_T[1:3, 1:3] .<= 1)
    #@constraint(model, estimated_T[1:3, 1:3] .>= -1)
    #@constraint(model, dot(center_3d,estimated_T[3, 1:3]) >=0)
    @constraint(model, sum(estimated_T) == 1)
    #@variable(model, R_abs[1:3, 1:3] >= 0 )
    #@constraint(model, R_abs .>= estimated_T[1:3, 1:3])
    #@constraint(model, R_abs .>= -estimated_T[1:3, 1:3])
    #for i in 1:3
    #  @constraint(model, [1 1 1] * R_abs[:, i] .<= sqrt(3))
    #  @constraint(model, [1 1 1] * R_abs[i, :] .<= sqrt(3))
    #end

    # switch constraints
    @variable(model, switch[1:num_3d_points, 1:num_2d_points], Bin)
    initial_switch = ones(num_3d_points, num_2d_points) - Matrix(1.0I, num_3d_points, num_3d_points)
    @constraint(model, switch .== initial_switch)
    #for i = 1:num_3d_points
    #    @constraint(model, sum(switch[i,:]) == num_2d_points-1)
    #end
    #for i = 1:num_2d_points
    #    @constraint(model, sum(switch[:,i]) == num_3d_points-1)
    #end

    # alpha constraint
    @variable(model, alpha[1:num_3d_points, 1:num_2d_points, 1:2])
    @constraint(model, alpha .>= 0)

    # cost function constraint
    for i = 1:num_3d_points
        for j = 1:num_2d_points
          a = estimated_T[3,:]'*(plucker_3d[1:6,i].*norm_2d[1,j]) .- estimated_T[1,:]' * (plucker_3d[1:6,i].*norm_2d[3,j])
          b = estimated_T[3,:]'*(plucker_3d[1:6,i].*norm_2d[2,j]) .- estimated_T[2,:]' * (plucker_3d[1:6,i].*norm_2d[3,j])
          @constraint(model, a .- (1e8 * switch[i,j]) .<= alpha[i,j, 1])
          @constraint(model, a .+ (1e8 * switch[i,j]) .>= -alpha[i,j, 1])
          @constraint(model, b .- (1e8 * switch[i,j]) .<= alpha[i,j, 2])
          @constraint(model, b .+ (1e8 * switch[i,j]) .>= -alpha[i,j, 2])
          #l_i = P*estimated_T*plucker_3d[:,i]
          #x_j = xy_2d[1:3,j]
          #y_j = xy_2d[4:6,j]
          #@constraint(model, x_j'*l_i .- (1e8 * switch[i,j]) .<= alpha[i,j, 1])
          #@constraint(model, x_j'*l_i .+ (1e8 * switch[i,j]) .>= -alpha[i,j, 1])
          #@constraint(model, y_j'*l_i .- (1e8 * switch[i,j]) .<= alpha[i,j, 2])
          #@constraint(model, y_j'*l_i .+ (1e8 * switch[i,j]) .>= -alpha[i,j, 2])
        end
    end
    @objective(model, Min, sum(switch)+sum(alpha))

    println("\nOptimizing")
    start = time_ns()
    JuMP.optimize!(model)
    stop = time_ns()
    time = stop-start;

    println("Solution is:")
    #println(JuMP.value.(switch))
    #println(JuMP.value.(alpha))
    println(JuMP.termination_status(model))

    final_T = JuMP.value.(estimated_T)
    final_switch = JuMP.value.(switch)
    println(sum(final_switch.==0))
    associations = zeros(num_3d_points, 1)
    for i = 1:num_3d_points
      for j = 1:num_2d_points
        if final_switch[i,j] .== 0
          associations[i] = j
        end
      end
    end

    final_R = FitRotation(final_T[1:3,1:3])

    final_skew_t = final_T[1:3,4:6]*final_R'
    final_t = SkewToVec(final_skew_t)

    println(final_R)
    println(final_t)

    println(associations)

    return final_R, final_t - closest_point, time/1e6, num_3d_points*num_2d_points, associations

end

lines_3d = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.l3d"; header=false, delim=' ', ignorerepeated=true))
associations = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.nview-lines"; header=false, delim=' ', ignorerepeated=true, missingstring="*"))

df = DataFrame(time_ms = Float64[], num_points= Int[], rad_error=Float64[], m_error=Float64[], association_error = Int[], frame_id=Int[])

for frame = 0:1:0
  for num = 69:2:69
    for itt = 1:10
      string_num = @sprintf "%03d" frame
      p_file = string("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.", string_num, ".P")
      P = Matrix(CSV.read(p_file; header=false, delim =' ', ignorerepeated=true))
      K, R, t = KRfromP(P)
      lines_file = string("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.", string_num, ".lines")
      lines_2d = Matrix(CSV.read(lines_file; header=false, delim=' ', ignorerepeated=true))
      perm = sample(1:size(lines_3d,1), num, replace = false)
      est_R, est_t, time, complexity, a= LinePoseEst( lines_3d[1:69,:], lines_2d[associations[:,frame+1],:], K)
      push!(df,[time, num, AngleAxis(est_R'*R).theta, norm(t-est_t), sum(a.==collect(1:num)), frame])
      CSV.write("2d_3d_size_results.csv",df)
    end
  end
end

p1 = plot(df, x="num_points", y="time_ms", Geom.boxplot(suppress_outliers=true))
p2 = plot(df, x="num_points", y="rad_error", Geom.boxplot(suppress_outliers=true))
p3 = plot(df, x="num_points", y="m_error", Geom.boxplot(suppress_outliers=true))
p4 = plot(df, x="num_points", y="association_error", Geom.boxplot(suppress_outliers=true))
draw(PNG("2d_3d_size_results.png", 8inch, 8inch),vstack(hstack(p1,p2),hstack(p3,p4)))


