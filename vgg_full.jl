using CSV, DataFrames, Printf, StatsBase, Rotations, LinearAlgebra, Images

include("mip_est.jl")
include("results_figure.jl")
include("pribyl_et_al.jl")

lines_3d = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.l3d"; header=false, delim=' ', ignorerepeated=true))
associations = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.nview-lines"; header=false, delim=' ', ignorerepeated=true, missingstring="*"))

df = DataFrame(time_ms = Float64[], rad_error=Float64[], m_error=Float64[], frame_id=Int[], constraint_method=String[])

for frame = 0:1:0
  indices = .!isequal.(associations[:,frame+1], missing)
  string_num = @sprintf "%03d" frame
  p_file = string("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.", string_num, ".P")
  P = Matrix(CSV.read(p_file; header=false, delim =' ', ignorerepeated=true))
  K, R, t = KRfromP(P)
  R = R
  println(K)
  lines_file = string("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.", string_num, ".lines")
  lines_2d = Matrix(CSV.read(lines_file; header=false, delim=' ', ignorerepeated=true))
  image_file = string("/home/stevenparkison/datasets/vgg_dataset/corridor/bt.", string_num, ".pgm")
  image = load(image_file)
  image = reinterpret(Gray{N0f8}, image)
  draw_results_figure(image, lines_2d, string("results/vgg/full/corridor/2d", string_num, ".jpg"))
  # Pribyle
  est_R, est_t, norm_r, time, complexity, a= PribylEtAl( lines_3d[indices,:], lines_2d[associations[indices,frame+1].+1,:], K, R, t)
  push!(df,[time, AngleAxis((est_R)'*R).theta, norm(t-est_t), frame, "Pribyl"])
  CSV.write("results/vgg/full/corridor/results.csv",df)
  println("GT vs Estim")
  println(hcat(R,t))
  println(hcat(est_R,est_t))
  draw_results_figure_3d(image, lines_2d[associations[indices,frame+1].+1,:], lines_3d[indices, :], a, K, est_R, est_R*-est_t, string("results/vgg/full/corridor/pribyl_lines_frame", string_num, ".jpg"))
  # Linear
  est_R, est_t, norm_r, time, complexity, a= MipEst( lines_3d[indices,:], lines_2d[associations[indices,frame+1].+1,:], K, R, t; switch_constraint = :Fixed)
  push!(df,[time, AngleAxis((est_R)'*R).theta, norm(t-est_t), frame, "Fixed Switch"])
  CSV.write("results/vgg/full/corridor/results.csv",df)
  println("GT vs Estim")
  println(hcat(R,t))
  println(hcat(est_R,est_t))
  draw_results_figure_3d(image, lines_2d[associations[indices,frame+1].+1,:], lines_3d[indices,:], a, K, est_R, est_R*-est_t, string("results/vgg/full/corridor/fixed_switch_lines_frame", string_num, ".jpg"))
end

lines_3d = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/MertonCollege1/l3d"; header=false, delim=' ', ignorerepeated=true))
associations = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/MertonCollege1/nview-lines"; header=false, delim=' ', ignorerepeated=true, missingstring="*"))

df = DataFrame(time_ms = Float64[], rad_error=Float64[], m_error=Float64[], frame_id=Int[], constraint_method=String[])

for frame = 1:1:0
  indices = .!isequal.(associations[:,frame], missing)
  string_num = @sprintf "%03d" frame
  p_file = string("/home/stevenparkison/datasets/vgg_dataset/MertonCollege1/", string_num, ".P")
  P = Matrix(CSV.read(p_file; header=false, delim =' ', ignorerepeated=true))
  K, R, t = KRfromP(P)
  R = R
  println(K)
  lines_file = string("/home/stevenparkison/datasets/vgg_dataset/MertonCollege1/", string_num, ".lines")
  lines_2d = Matrix(CSV.read(lines_file; header=false, delim=' ', ignorerepeated=true))
  image_file = string("/home/stevenparkison/datasets/vgg_dataset/MertonCollege1/", string_num, ".jpg")
  image = load(image_file)
  draw_results_figure(image, lines_2d, string("results/vgg/full/merton2/2d", string_num, ".jpg"))
  # Pribyle
  est_R, est_t, norm_r, time, complexity, a= PribylEtAl( lines_3d[indices,:], lines_2d[associations[indices,frame].+1,:], K, R, t)
  push!(df,[time, AngleAxis((est_R)'*R).theta, norm(t-est_t), frame, "Pribyl"])
  CSV.write("results/vgg/full/merton1/results.csv",df)
  println("GT vs Estim")
  println(hcat(R,t))
  println(hcat(est_R,est_t))
  draw_results_figure_3d(image, lines_2d[associations[indices,frame].+1,:], lines_3d[indices,:], a, K, est_R, est_R*-est_t, string("results/vgg/full/merton1/pribyl_lines_frame", string_num, ".jpg"), width = 1024, height = 768)
  # Linear
  est_R, est_t, norm_r, time, complexity, a= MipEst( lines_3d[indices,:], lines_2d[associations[indices,frame].+1,:], K, R, t; switch_constraint = :Fixed, z_direction = -1, image_width = 1024, image_height = 768, offset = 70)
  push!(df,[time, AngleAxis((est_R)'*R).theta, norm(t-est_t), frame, "Fixed Switch"])
  CSV.write("results/vgg/full/merton1/results.csv",df)
  println("GT vs Estim")
  println(hcat(R,t))
  println(hcat(est_R,est_t))
  draw_results_figure_3d(image, lines_2d[associations[indices,frame].+1,:], lines_3d[indices,:], a, K, est_R, est_R*-est_t, string("results/vgg/full/merton1/fixed_switch_lines_frame", string_num, ".jpg"), width = 1024, height = 768)
end

lines_3d = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/MertonCollege2/l3d"; header=false, delim=' ', ignorerepeated=true))
associations = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/MertonCollege2/nview-lines"; header=false, delim=' ', ignorerepeated=true, missingstring="*"))

df = DataFrame(time_ms = Float64[], rad_error=Float64[], m_error=Float64[], frame_id=Int[], constraint_method=String[])

for frame = 1:1:0
  indices = .!isequal.(associations[:,frame], missing)
  string_num = @sprintf "%03d" frame
  p_file = string("/home/stevenparkison/datasets/vgg_dataset/MertonCollege2/", string_num, ".P")
  P = Matrix(CSV.read(p_file; header=false, delim =' ', ignorerepeated=true))
  K, R, t = KRfromP(P)
  R = R
  println(K)
  lines_file = string("/home/stevenparkison/datasets/vgg_dataset/MertonCollege2/", string_num, ".lines")
  lines_2d = Matrix(CSV.read(lines_file; header=false, delim=' ', ignorerepeated=true))
  image_file = string("/home/stevenparkison/datasets/vgg_dataset/MertonCollege2/", string_num, ".jpg")
  image = load(image_file)
  draw_results_figure(image, lines_2d, string("results/vgg/full/merton2/2d", string_num, ".jpg"))
  # Pribyle
  est_R, est_t, norm_r, time, complexity, a= PribylEtAl( lines_3d[indices,:], lines_2d[associations[indices,frame].+1,:], K, R, t)
  push!(df,[time, AngleAxis((est_R)'*R).theta, norm(t-est_t), frame, "Pribyl"])
  CSV.write("results/vgg/full/merton2/results.csv",df)
  println("GT vs Estim")
  println(hcat(R,t))
  println(hcat(est_R,est_t))
  draw_results_figure_3d(image, lines_2d[associations[indices,frame].+1,:], lines_3d[indices,:], a, K, est_R, est_R*-est_t, string("results/vgg/full/merton2/pribyl_lines_frame", string_num, ".jpg"), width = 1024, height = 768)
  # Linear
  est_R, est_t, norm_r, time, complexity, a= MipEst( lines_3d[indices,:], lines_2d[associations[indices,frame].+1,:], K, R, t; switch_constraint = :Fixed, z_direction = -1, image_width = 1024, image_height = 768, offset = 70)
  push!(df,[time, AngleAxis((est_R)'*R).theta, norm(t-est_t), frame, "Fixed Switch"])
  CSV.write("results/vgg/full/merton2/results.csv",df)
  println("GT vs Estim")
  println(hcat(R,t))
  println(hcat(est_R,est_t))
  draw_results_figure_3d(image, lines_2d[associations[indices,frame].+1,:], lines_3d[indices,:], a, K, est_R, est_R*-est_t, string("results/vgg/full/merton2/fixed_switch_lines_frame", string_num, ".jpg"), width = 1024, height = 768)
end

lines_3d = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/MertonCollege3/l3d"; header=false, delim=' ', ignorerepeated=true))
associations = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/MertonCollege3/nview-lines"; header=false, delim=' ', ignorerepeated=true, missingstring="*"))

df = DataFrame(time_ms = Float64[], rad_error=Float64[], m_error=Float64[], frame_id=Int[], constraint_method=String[])

for frame = 1:1:0
  indices = .!isequal.(associations[:,frame], missing)
  string_num = @sprintf "%03d" frame
  p_file = string("/home/stevenparkison/datasets/vgg_dataset/MertonCollege3/", string_num, ".P")
  P = Matrix(CSV.read(p_file; header=false, delim =' ', ignorerepeated=true))
  K, R, t = KRfromP(P)
  R = R
  println(K)
  lines_file = string("/home/stevenparkison/datasets/vgg_dataset/MertonCollege3/", string_num, ".lines")
  lines_2d = Matrix(CSV.read(lines_file; header=false, delim=' ', ignorerepeated=true))
  image_file = string("/home/stevenparkison/datasets/vgg_dataset/MertonCollege3/", string_num, ".jpg")
  image = load(image_file)
  draw_results_figure(image, lines_2d, string("results/vgg/full/merton3/2d", string_num, ".jpg"))
  # Pribyle
  est_R, est_t, norm_r, time, complexity, a= PribylEtAl( lines_3d[indices,:], lines_2d[associations[indices,frame].+1,:], K, R, t)
  push!(df,[time, AngleAxis((est_R)'*R).theta, norm(t-est_t), frame, "Pribyl"])
  CSV.write("results/vgg/full/merton3/results.csv",df)
  println("GT vs Estim")
  println(hcat(R,t))
  println(hcat(est_R,est_t))
  draw_results_figure_3d(image, lines_2d[associations[indices,frame].+1,:], lines_3d[indices,:], a, K, est_R, est_R*-est_t, string("results/vgg/full/merton3/pribyl_lines_frame", string_num, ".jpg"), width = 1024, height = 768)
  # Linear
  est_R, est_t, norm_r, time, complexity, a= MipEst( lines_3d[indices,:], lines_2d[associations[indices,frame].+1,:], K, R, t; switch_constraint = :Fixed, z_direction = -1, image_width = 1024, image_height = 768, offset = 70)
  push!(df,[time, AngleAxis((est_R)'*R).theta, norm(t-est_t), frame, "Fixed Switch"])
  CSV.write("results/vgg/full/merton3/results.csv",df)
  println("GT vs Estim")
  println(hcat(R,t))
  println(hcat(est_R,est_t))
  draw_results_figure_3d(image, lines_2d[associations[indices,frame].+1,:], lines_3d[indices,:], a, K, est_R, est_R*-est_t, string("results/vgg/full/merton3/fixed_switch_lines_frame", string_num, ".jpg"), width = 1024, height = 768)
end

lines_3d = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/UniversityLibrary/l3d"; header=false, delim=' ', ignorerepeated=true))
associations = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/UniversityLibrary/nview-lines"; header=false, delim=' ', ignorerepeated=true, missingstring="*"))

df = DataFrame(time_ms = Float64[], rad_error=Float64[], m_error=Float64[], frame_id=Int[], constraint_method=String[])

for frame = 1:1:0
  indices = .!isequal.(associations[:,frame], missing)
  string_num = @sprintf "%03d" frame
  p_file = string("/home/stevenparkison/datasets/vgg_dataset/UniversityLibrary/", string_num, ".P")
  P = Matrix(CSV.read(p_file; header=false, delim =' ', ignorerepeated=true))
  K, R, t = KRfromP(P)
  R = R
  println(K)
  lines_file = string("/home/stevenparkison/datasets/vgg_dataset/UniversityLibrary/", string_num, ".lines")
  lines_2d = Matrix(CSV.read(lines_file; header=false, delim=' ', ignorerepeated=true))
  image_file = string("/home/stevenparkison/datasets/vgg_dataset/UniversityLibrary/", string_num, ".jpg")
  image = load(image_file)
  draw_results_figure(image, lines_2d, string("results/vgg/full/library/2d", string_num, ".jpg"))
  # Pribyle
  est_R, est_t, norm_r, time, complexity, a= PribylEtAl( lines_3d[indices,:], lines_2d[associations[indices,frame].+1,:], K, R, t)
  push!(df,[time, AngleAxis((est_R)'*R).theta, norm(t-est_t), frame, "Pribyl"])
  CSV.write("results/vgg/full/library/results.csv",df)
  println("GT vs Estim")
  println(hcat(R,t))
  println(hcat(est_R,est_t))
  draw_results_figure_3d(image, lines_2d[associations[indices,frame].+1,:], lines_3d[indices,:], a, K, est_R, est_R*-est_t, string("results/vgg/full/library/pribyl_lines_frame", string_num, ".jpg"), width = 1024, height = 768)
  # Linear
  est_R, est_t, norm_r, time, complexity, a= MipEst( lines_3d[indices,:], lines_2d[associations[indices,frame].+1,:], K, R, t; switch_constraint = :Fixed, z_direction = -1, image_width = 1024, image_height = 768, offset = 80)
  push!(df,[time, AngleAxis((est_R)'*R).theta, norm(t-est_t), frame, "Fixed Switch"])
  CSV.write("results/vgg/full/library/results.csv",df)
  println("GT vs Estim")
  println(hcat(R,t))
  println(hcat(est_R,est_t))
  draw_results_figure_3d(image, lines_2d[associations[indices,frame].+1,:], lines_3d[indices,:], a, K, est_R, est_R*-est_t, string("results/vgg/full/library/fixed_switch_lines_frame", string_num, ".jpg"), width = 1024, height = 768)
end

lines_3d = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/WadhamCollege/l3d"; header=false, delim=' ', ignorerepeated=true))
associations = Matrix(CSV.read("/home/stevenparkison/datasets/vgg_dataset/WadhamCollege/nview-lines"; header=false, delim=' ', ignorerepeated=true, missingstring="*"))

df = DataFrame(time_ms = Float64[], rad_error=Float64[], m_error=Float64[], frame_id=Int[], constraint_method=String[])

for frame = 1:1:5
  indices = .!isequal.(associations[:,frame], missing)
  string_num = @sprintf "%03d" frame
  p_file = string("/home/stevenparkison/datasets/vgg_dataset/WadhamCollege/", string_num, ".P")
  P = Matrix(CSV.read(p_file; header=false, delim =' ', ignorerepeated=true))
  K, R, t = KRfromP(P)
  R = R
  println(K)
  lines_file = string("/home/stevenparkison/datasets/vgg_dataset/WadhamCollege/", string_num, ".lines")
  lines_2d = Matrix(CSV.read(lines_file; header=false, delim=' ', ignorerepeated=true))
  image_file = string("/home/stevenparkison/datasets/vgg_dataset/WadhamCollege/", string_num, ".jpg")
  image = load(image_file)
  draw_results_figure(image, lines_2d, string("results/vgg/full/wadham/2d", string_num, ".jpg"))
  # Pribyle
  est_R, est_t, norm_r, time, complexity, a= PribylEtAl( lines_3d[indices,:], lines_2d[associations[indices,frame].+1,:], K, R, t; z_direction = 1)
  push!(df,[time, AngleAxis((est_R)'*R).theta, norm(t-est_t), frame, "Pribyl"])
  CSV.write("results/vgg/full/wadham/results.csv",df)
  println("GT vs Estim")
  println(hcat(R,t))
  println(hcat(est_R,est_t))
  draw_results_figure_3d(image, lines_2d[associations[indices,frame].+1,:], lines_3d[indices,:], a, K, est_R, est_R*-est_t, string("results/vgg/full/wadham/pribyl_lines_frame", string_num, ".jpg"), width = 1024, height = 768, z_direction = -1)
  # Linear
  est_R, est_t, norm_r, time, complexity, a= MipEst( lines_3d[indices,:], lines_2d[associations[indices,frame].+1,:], K, R, t; switch_constraint = :Fixed, z_direction = 1, image_width = 1024, image_height = 768, offset = 70)
  push!(df,[time, AngleAxis((est_R)'*R).theta, norm(t-est_t), frame, "Fixed Switch"])
  CSV.write("results/vgg/full/wadham/results.csv",df)
  println("GT vs Estim")
  println(hcat(R,t))
  println(hcat(est_R,est_t))
  draw_results_figure_3d(image, lines_2d[associations[indices,frame].+1,:], lines_3d[indices,:], a, K, est_R, est_R*-est_t, string("results/vgg/full/wadham/fixed_switch_lines_frame", string_num, ".jpg"), width = 1024, height = 768, z_direction = -1)
end
