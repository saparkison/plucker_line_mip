using LinearAlgebra

function HomogenousLines( line_2d_endpts, K )
  num_2d_points = size(line_2d_endpts,1)
  norm_2d = zeros(6, num_2d_points)
  for i = 1:num_2d_points
    point_a = inv(K) * vcat(line_2d_endpts[i,1:2], [1])
    point_b = inv(K) * vcat(line_2d_endpts[i,3:4], [1])
    norm_2d[1:3,i] = LinearAlgebra.cross( point_a, point_b )
    norm_2d[4:5,i] = point_a[1:2] - point_b[1:2]
    norm_2d[6,i] = 0
  end

  return norm_2d
end
