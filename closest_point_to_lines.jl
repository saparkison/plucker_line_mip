using LinearAlgebra

function ClosestPointToLines(line_3D_end_pts)

  n_lines = size(line_3D_end_pts,1)
  w = ones(n_lines, 1)

  pts_start = line_3D_end_pts[:, 1:3]'
  pts_end = line_3D_end_pts[:, 4:6]'

  e = pts_end - pts_start
  for i = 1:n_lines
    e[:,i] = normalize(e[:,i], 2)
  end

  A = repeat(e[:], 1, 3) .* kron(e', ones(3,1))
  M = repeat(Matrix(1.0I, 3, 3), n_lines, 1) - A

  M = ((kron(w, ones(3,3))).^0.5) .* M
  temp_kron = kron(pts_start', ones(3,1))
  temp_dot = ones(n_lines*3,1)
  for i = 1:(n_lines*3)
    temp_dot[i,1] = dot(M[i,:],temp_kron[i,:])
  end
  c = ((kron(w,ones(3,1))).^0.5) .* temp_dot

  return pinv(M'*M)*(M'*c)
end
