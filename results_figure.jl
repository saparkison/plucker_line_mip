using Images, ImageDraw, ImageInTerminal, FileIO, Colors

function draw_results_figure(img::AbstractArray, line_2d_endpts, file_name::String)
  new_img = RGB.(img)
  results_figure_plot(new_img, line_2d_endpts, RGB{N0f8}(1.0, 0.0, 0.0))
  save(file_name, new_img)
end

function draw_results_figure_3d(img::AbstractArray, line_2d_endpts, line_3d_endpts, associations, K, R, t, file_name::String; height::Int = 511, width::Int = 511, z_direction = 1)
  line_2d = Array{Float64,2}(undef, 0, 4)
  link = Array{Float64, 2}(undef, 0, 4)
  for i = 1:size(line_3d_endpts,1)
    println(R*line_3d_endpts[i,1:3] + t)
    temp_a = K*(R*line_3d_endpts[i,1:3] + t)
    temp_b = K*(R*line_3d_endpts[i,4:6] + t)
    if(temp_a[3]*z_direction > 0 && temp_b[3]*z_direction > 0)
      pixel_a = temp_a[1:2]./temp_a[3]
      pixel_b = temp_b[1:2]./temp_b[3]
      if(pixel_a[1] > 1 && pixel_a[1] <= width && pixel_a[2] > 1 && pixel_a[2] <= height)
        if(pixel_b[1] > 1 && pixel_b[1] <= width && pixel_b[2] > 1 && pixel_b[2] <= height)
          line_2d = vcat(line_2d, [pixel_a' pixel_b'])
          if(associations[i] >0)
            link = vcat(link, [(pixel_a+pixel_b)'/2.0 (line_2d_endpts[associations[i], 1:2]+line_2d_endpts[associations[i],3:4])'/2.0])
          end
        end
      end
    end
  end
  new_img = RGB.(img)
  results_figure_plot(new_img, line_2d_endpts, RGB{N0f8}(1.0, 0.0, 0.0))
  results_figure_plot(new_img, line_2d, RGB{N0f8}(0.0, 1.0, 0.0))
  results_figure_plot(new_img, link, RGB{N0f8}(0.0, 0.0, 1.0))
  save(file_name, new_img)
end

function results_figure_plot(img::AbstractArray, line_2d_endpts, color::RGB{N0f8})
  line_2d_int = round.(Int64, line_2d_endpts)
  for i = 1:size(line_2d_endpts,1)
    draw!(img, LineSegment(Point(line_2d_int[i,1], line_2d_int[i,2]), Point(line_2d_int[i,3], line_2d_int[i,4])), color)
  end
end
