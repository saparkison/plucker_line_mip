using JuMP, Gurobi, Profile, CSV, LinearAlgebra, Rotations, DataFrames, Gadfly, Cairo, Fontconfig, StatsBase

function perm_search(data_size::Int, noise::Float64, num_outlier::Int = 0)

  data = rand(3,data_size)
  similarity_matrix = zeros(data_size,data_size)

  for i = 1:data_size
    for j = 1:data_size
      similarity_matrix[i,j] = norm(data[:,i]-data[:,j])
    end
  end

  perm = sample(1:data_size, data_size, replace = false)
  new_data = data[:,perm] + rand(3,data_size) .*noise
  new_data = hcat(new_data, rand(3,num_outlier))
  new_data_size = data_size + num_outlier
  new_similarity_matrix = zeros(new_data_size,new_data_size)
  for i = 1:new_data_size
    for j = 1:new_data_size
      new_similarity_matrix[i,j] = norm(new_data[:,i]-new_data[:,j])
    end
  end

  start = time_ns()
  optimizer = Gurobi.Optimizer
  model = Model(with_optimizer(optimizer, Threads = 50))
  @variable(model, alpha[1:data_size,1:data_size])
  @variable(model, switch[1:data_size, 1:new_data_size], Bin)
  @objective(model, Min, sum(alpha))

  # switch constraints
  for i = 1:data_size
      @constraint(model, sum(switch[i,:]) == new_data_size-1)
  end
  for i = 1:new_data_size
      @constraint(model, sum(switch[:,i]) == data_size-1)
  end


  # alpha constraint, slack variable
  @constraint(model, alpha .>= 0)
  for i = 1:data_size
    for j = 1:data_size
      for new_i = 1:new_data_size
        for new_j = 1:new_data_size
          @constraint(model, (1000*(switch[i,new_i]+switch[j,new_j])) + alpha[i,j] >= similarity_matrix[i,j]-new_similarity_matrix[new_i,new_j] )
          @constraint(model, -(1000*(switch[i,new_i]+switch[j,new_j])) - alpha[i,j] <= similarity_matrix[i,j]-new_similarity_matrix[new_i,new_j] )
        end
      end
    end
  end

  println("\nOptimizing")
  JuMP.optimize!(model)
  stop = time_ns()
  println("Elaped time")
  println((stop-start)/1e6)

  final_switch = JuMP.value.(switch)
  associations = zeros(data_size, 1)
  for i = 1:data_size
    for j = 1:data_size
      if final_switch[i,j] < 1e-4
        associations[j] = i
      end
    end
  end
  println(perm)
  println(associations)

  return (stop-start)/1e6, data_size - sum(associations.==perm)
end


df = DataFrame(time_ms = Float64[], num_points= Int[])

for num = 10:10:100
  for i = 1:10
    time, error = perm_search(num, 1e-4)
    push!(df, [time, num])
    CSV.write("perm_size_results.csv", df)
  end
end


p1 = plot(df, x="num_points", y="time_ms", Geom.boxplot(suppress_outliers=true))
draw(PNG("perm_results.png", 8inch, 8inch),p1)

df = DataFrame(time_ms = Float64[], num_outliers= Int[], errors = Int[])
for num = 10:5:50
  for i = 1:10
    time, error = perm_search(15, 1e-4, num)
    push!(df, [time, num, error])
    CSV.write("perm_outlier_results.csv", df)
  end
end

p = plot(df, x="num_outliers", y="time_ms", Geom.boxplot(suppress_outliers=true))
draw(PNG("perm_outliers_results.png", 8inch, 8inch),p)
